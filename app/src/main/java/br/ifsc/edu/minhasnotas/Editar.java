package br.ifsc.edu.minhasnotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Editar extends AppCompatActivity {

    String texto, titulo;
    EditText editText;
    TextView textView;
    NotasController notasController;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);
        editText = findViewById(R.id.editTextEditar);
        textView = findViewById(R.id.textViewTitulo);
        notasController = new NotasController(getApplicationContext());

        id = (int) getIntent().getSerializableExtra("notas");
        titulo =notasController.recuperaTitulo(id);
        texto = notasController.recuperaTexto(id);

        editText.setText(texto);
        textView.setText(titulo);


    }

    public void salvar2(View view) {
        notasController.updateNota(editText.getText().toString(), id);
        Intent intent = new Intent(Editar.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        notasController.updateNota(editText.getText().toString(),id);


    }
}
