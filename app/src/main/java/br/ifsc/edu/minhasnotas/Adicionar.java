package br.ifsc.edu.minhasnotas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Adicionar extends AppCompatActivity {

    NotasController notasController;
    EditText titulo, texto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionarr);
        titulo = findViewById(R.id.editTextTitulo);
        texto = findViewById(R.id.editTextTexto);
        notasController = new NotasController(getApplicationContext());
    }

    public void salvar (View v){
        if (titulo.getText().toString().trim().equals("") || texto.getText().toString().trim().equals("")){
            Toast.makeText(getApplicationContext(), "PREENCHA OS CAMPOS", Toast.LENGTH_LONG).show();
        }else{
            String titulotxt = titulo.getText().toString();
            String textotxt = texto.getText().toString();
            notasController.addNota(titulotxt,textotxt);
            Toast.makeText(getApplicationContext(), "NOTA ADICIDONADA COM SUCESSO, RETORNANDO A VISUALIZAÇÃO", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Adicionar.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
