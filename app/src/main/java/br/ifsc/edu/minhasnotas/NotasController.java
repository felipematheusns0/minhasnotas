package br.ifsc.edu.minhasnotas;

import android.content.Context;

import java.util.ArrayList;

class NotasController {

    Context mContext ;
    NotasDAO notasDAO;
    public NotasController(Context c) {
        mContext=c;
        notasDAO= new NotasDAO(mContext);


    }

    public ArrayList<String> getTitulo() {
        ArrayList<String> resultado = new ArrayList<String>();

        for (Nota n:notasDAO.getNotas()){
            resultado.add(n.getTitulo());
        }

        return resultado;
    }

    public ArrayList<Nota> notas(){
        ArrayList<Nota> resultado = new ArrayList<Nota>();

        for (Nota n:notasDAO.getNotas()){
            resultado.add(n);
        }
        return resultado;
    }

    public void addNota(String titulo, String texto) {
        notasDAO.inserirNotas(titulo, texto);
    }

    public String recuperaTitulo(int id) {
        return notasDAO.titulo(id);
    }

    public void updateNota(String texto, int id){
        notasDAO.update(texto, id);
    }

    public String recuperaTexto(int id) {
        return notasDAO.texto(id);
    }
}
