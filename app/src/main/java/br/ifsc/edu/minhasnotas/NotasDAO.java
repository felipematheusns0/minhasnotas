package br.ifsc.edu.minhasnotas;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.EditText;

import java.util.ArrayList;

class NotasDAO {
    SQLiteDatabase bd;

    public NotasDAO(Context c) {

        bd=c.openOrCreateDatabase("NotasDB", Context.MODE_PRIVATE, null);

        bd.execSQL("CREATE TABLE IF NOT EXISTS notas (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "titulo VARCHAR NOT NULL," +
                "texto VARCHAR);");


    }

    public ArrayList<Nota> getNotas() {
        ArrayList<Nota> resultado = new ArrayList<Nota>();

        Cursor cursor = bd.rawQuery("SELECT titulo, id, texto FROM notas", null, null);
        cursor.moveToFirst();
        String titulo, texto;
        int id;

        while (!cursor.isAfterLast()){
            id = cursor.getInt(cursor.getColumnIndex("id"));
            titulo = cursor.getString(cursor.getColumnIndex("titulo"));
            texto = cursor.getString(cursor.getColumnIndex("texto"));
            resultado.add(new Nota(titulo, texto, id));
            cursor.moveToNext();
        }

        return resultado;
    }


    public void inserirNotas(String titulo, String texto) {
        bd.execSQL("INSERT INTO notas (titulo, texto) VALUES ('"+titulo+"', '"+texto+"');");
    }

    public String titulo(int id) {
        Cursor cursor;
        cursor = bd.rawQuery("SELECT titulo FROM notas WHERE id = "+id+";", null);
        cursor.moveToFirst();
        String titulo = cursor.getString(cursor.getColumnIndex("titulo"));
        return titulo;
    }

    public String texto(int id) {
        Cursor cursor;
        cursor = bd.rawQuery("SELECT texto FROM notas WHERE id = "+id+";", null);
        cursor.moveToFirst();
        String texto = cursor.getString(cursor.getColumnIndex("texto"));
        return texto;
    }

    public void update(String texto, int id) {
        bd.execSQL("UPDATE notas SET texto = '"+texto+"' WHERE id = "+id+";");
    }
}
