package br.ifsc.edu.minhasnotas;

import java.io.Serializable;

class Nota implements Serializable {
    String titulo, texto;
    int id;

    public Nota(String titulo, String texto, int id) {
        this.titulo = titulo;
        this.texto = texto;
        this.id = id;
    }
    public Nota(){

    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
